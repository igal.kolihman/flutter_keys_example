import 'package:flutter/material.dart';
import 'package:random_color/random_color.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: PositionedBoxes());
  }
}

class PositionedBoxes extends StatefulWidget {
  @override
  _PositionedBoxesState createState() => _PositionedBoxesState();
}

class _PositionedBoxesState extends State<PositionedBoxes> {
  List<Widget> tiles;

  swapTiles() {
    setState(() {
      tiles.insert(1, tiles.removeAt(0));
    });
  }

  void initState() {
    super.initState();
    tiles = [
      StatefulColorfulBox(key: UniqueKey()),
      StatefulColorfulBox(key: UniqueKey()),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: tiles,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.sync),
        onPressed: swapTiles,
      ),
    );
  }
}

class StatefulColorfulBox extends StatefulWidget {
  StatefulColorfulBox({Key key}) : super(key: key);

  @override
  _StatefulColorfulBoxState createState() => _StatefulColorfulBoxState();
}

class _StatefulColorfulBoxState extends State<StatefulColorfulBox> {
  RandomColor _randomColor = RandomColor();

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: new BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: _randomColor.randomColor(),
      ),
      width: 100,
      height: 100,
    );
  }
}
