# flutter_keys_example

An example project for simple use of keys in flutter.

## Description

This example demonstrates a simple use of flutter keys. All that we do is switching places of the two boxes by pressing the FloatingActionButton at the bottom-right corner.


Before Switch                   |  After Switching
--------------------------------|------------------------------
![](images/before_switch.jpg)   |  ![](images/after_switch.jpg) 